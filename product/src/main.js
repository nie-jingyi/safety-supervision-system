import Vue from "vue";
import App from "./App.vue";
import router from "./router";
import store from "./store";
import vuetify from "./plugins/vuetify";
// 引入Message提醒
import Message from "./utils/notification/index.js";
import axios from "axios";

Vue.config.productionTip = false;

Vue.prototype.$message = Message;
Vue.prototype.$axios = axios;

axios.defaults.baseURL = 'http://39.105.128.207:8080/'

axios.interceptors.request.use((config) => {
      // 判断是否存在token，如果存在的话，则每个http header都加上token
  let session = sessionStorage.getItem('session')
      if (session) {
        config.headers.ssid = session;
      }
      return config;
    },
    error => {
      return Promise.reject(error);
    });

router.beforeEach((to, from, next) => {
    if (to.path == '/login') next()
    else {
        if (!sessionStorage.getItem("session")) {
            next({path: '/login'})
        }
    }
    next();
});

new Vue({
  router,
  store,
  vuetify,
  render: (h) => h(App),
}).$mount("#app");
