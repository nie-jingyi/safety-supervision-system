import Vue from 'vue';
import VueRouter from 'vue-router';

Vue.use(VueRouter);

const routes = [
  //  重定向
  { path: '/', redirect: '/login' },
  //  登录
  { path: '/login', name: 'Login', component: () => import('../views/Login') },
  //  Client端
  //  个人中心
  {
    path: '/personalCenter',
    name: 'PersonalCenter',
    component: () => import('../views/Client/PersonalCenter/PersonalCenter'),
  },
  //  非会员课程
  {
    path: '/nonmemberCourse',
    name: 'NonmemberCourse',
    component: () => import('../views/Client/Courses/NonmemberCourse'),
  },
  //  初训课程
  {
    path: '/firstCourse',
    name: 'FirstCourse',
    component: () => import('../views/Client/Courses/FirstCourse'),
  },
  //  复训课程
  {
    path: '/secondCourse',
    name: 'SecondCourse',
    component: () => import('../views/Client/Courses/SecondCourse'),
  },
  //  专项课程
  {
    path: '/specialCourse',
    name: 'SpecialCourse',
    component: () => import('../views/Client/Courses/SpecialCourse'),
  },
  //课程详情页
  {
    path: '/course/:id/detail',
    name: 'CourseDetail',
    component: () => import('../views/Client/Courses/CourseDetail'),
    props: true,
  },
  //章节列表页
  {
    path: '/course/:id/chapter',
    name: 'CourseChapter',
    component: () => import('../views/Client/Courses/CourseChapter'),
    props: true,
  },
  //课程学习页
  {
    path: '/course/:id/learn/:leaf',
    name: 'CourseLearn',
    component: () => import('../views/Client/Courses/CourseLearn'),
    props: true,
  },

  //  会员缴费
  {
    path: '/memberPay',
    name: 'MemberPay',
    component: () => import('../views/Client/Pay/MemberPay'),
  },
  //  会员缴费-购物车
  {
    path: '/memberPay/chart',
    name: 'Chart',
    component: () => import('../views/Client/Pay/Chart'),
  },
  //  会员缴费-确认订单
  {
    path: '/memberPay/confirm',
    name: 'PaymentConfirm',
    component: () => import('../views/Client/Pay/PaymentConfirm'),
  },
  //  会员缴费-支付中
  {
    path: '/memberPay/ongoing',
    name: 'PaymentOngoing',
    component: () => import('../views/Client/Pay/PaymentOngoing'),
  },
  //  会员缴费-支付失败
  {
    path: '/memberPay/fail',
    name: 'PaymentFailure',
    component: () => import('../views/Client/Pay/PaymentFailure'),
  },
  //  会员缴费-支付成功
  {
    path: '/memberPay/success',
    name: 'PaymentSuccess',
    component: () => import('../views/Client/Pay/PaymentSuccess'),
  },
  //  系统公告列表页
  {
    path: '/systemNotice',
    name: 'SystemNotice',
    component: () => import('../views/Client/Notice/SystemNotice'),
  },
  //  系统公告详情页
  {
    // path: '/systemNotice/detail',
    path: '/systemNotice/detail:id',
    name: 'SystemNoticeDetail',
    component: () => import('../views/Client/Notice/SystemNoticeDetail'),
  },
  //  已选课程
  {
    path: '/selectedCourse',
    name: 'SelectedCourse',
    component: () => import('../views/Client/Courses/SelectedCourse'),
  },
  //  订单列表
  {
    path: '/orderList',
    name: 'OrderList',
    component: () => import('../views/Client/Orders/OrderList'),
  },
  //  发票备注填写
  {
    path: '/orderList/invoiceEdit:id',
    name: 'InvoiceEdit',
    component: () => import('../views/Client/Orders/InvoiceEdit'),
  },
  //初训,复训,专题考试页
  {
    path: '/exerciseExam/:id',
    name: 'ExerciseExam',
    component: () => import('../views/Client/Exam/ExerciseExam'),
  },
  //课程考试页
  {
    path: '/courseExam/:id',
    name: 'CourseExam',
    component: () => import('../views/Client/Exam/CourseExam'),
  },
  //初训,复训,专题考试题目页
  {
    path: '/exerciseExamContent/:id',
    name: 'ExerciseExamContent',
    component: () => import('../views/Client/Exam/ExerciseExamContent'),
  },
  //课程考试题目页
  {
    path: '/courseExamContent/:id',
    name: 'CourseExamContent',
    component: () => import('../views/Client/Exam/CourseExamContent'),
  },
  //结果页(内置于课程页)
  {
    path: '/courseResult/:id',
    name: 'CourseResult',
    component: () => import('../views/Client/Result/CourseResult'),
  },
  //答案页(内置于课程页)
  {
    path: '/courseAnswer/:id',
    name: 'CourseAnswer',
    component: () => import('../views/Client/Result/CourseAnswer'),
  },
  //结果页(初训_复训_专题)
  {
    path: '/exerciseResult/:id',
    name: 'ExerciseResult',
    component: () => import('../views/Client/Result/ExerciseResult'),
  },
  //答案页(初训_复训_专题)
  {
    path: '/exerciseAnswer/:id',
    name: 'ExerciseAnswer',
    component: () => import('../views/Client/Result/ExerciseAnswer'),
  },
  //  Manage端
  //  注册审核
  {
    path: '/registerJudge',
    name: 'RegisterJudge',
    component: () => import('../views/Manage/RegisterManagement/RegisterJudge'),
  },
  //  单位管理
  {
    path: '/companyManage',
    name: 'CompanyManage',
    component: () => import('../views/Manage/CompanyManagement/CompanyManage'),
  },
  {
    path: '/companyDetail',
    name: 'CompanyDetail',
    component: () => import('../views/Manage/CompanyManagement/CompanyDetail'),
  },
  //  课程管理、专题管理
  {
    path: '/courseManage',
    name: 'CourseManage',
    component: () => import('../views/Manage/CoursesManagement/CourseManage'),
  },
  {
    path: '/addCourse',
    name: 'AddCourse',
    component: () => import('../views/Manage/CoursesManagement/AddCourse'),
  },
  {
    path: '/addChapter/:id',
    name: 'AddChapter',
    component: () => import('../views/Manage/CoursesManagement/AddChapter'),
    props: true,
  },
  {
    path: '/specialCourseManage',
    name: 'SpecialCourseManage',
    component: () =>
      import('../views/Manage/CoursesManagement/SpecialCourseManage'),
  },
  {
    path: '/addSpecialCourse',
    name: 'AddSpecialCourse',
    component: () =>
      import('../views/Manage/CoursesManagement/AddSpecialCourse'),
  },
  //  试卷管理（初训、复训）
  {
    path: '/firstPaperManage',
    name: 'FirstPaperManage',
    component: () => import('../views/Manage/PaperManagement/FirstPaperManage'),
  },
  {
    path: '/secondPaperManage',
    name: 'SecondPaperManage',
    component: () =>
      import('../views/Manage/PaperManagement/SecondPaperManage'),
  },
  //  公告管理
  {
    path: '/noticeManage',
    name: 'NoticeManage',
    component: () => import('../views/Manage/NoticeManagement/NoticeManage'),
  },
  //  订单管理
  {
    path: '/ordersManage',
    name: 'OrdersManage',
    component: () => import('../views/Manage/OrdersManagement/OrdersManage'),
  },
  {
    path: '/ordersInfo/:id',
    name: 'OrdersInfo',
    component: () => import('../views/Manage/OrdersManagement/OrdersInfo'),
  },
  //  题库管理
  {
    path: '/questionsManage',
    name: 'QuestionsManage',
    component: () =>
      import('../views/Manage/QuestionsManagement/QuestionsManage'),
  },
  //  创建试题
  {
    path: '/questionsCreate',
    name: 'CreateQuestion',
    component: () =>
      import('../views/Manage/QuestionsManagement/CreateQuestion'),
  },
  // 创建课程考试
  {
    path: '/courseExamCreate',
    name: 'CourseExamCreate',
    component: () => import('../views/Manage/ExamManagement/CourseExam'),
  },
  // 创建专题考试
  {
    path: '/subjectExamCreate',
    name: 'SubjectExamCreate',
    component: () => import('../views/Manage/ExamManagement/SubjectExam'),
  },
];

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes,
});

export default router;
