import Vue from "vue";
import Vuex from "vuex";
import Notification from "./module/notification.js";

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    resources: 'http://39.105.128.207:8080/resources/',
  },
  getters: {},
  mutations: {},
  actions: {},
  modules: {
    message: Notification,
  },
});
